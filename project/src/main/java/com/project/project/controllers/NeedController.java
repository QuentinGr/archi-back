package com.project.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;


import com.project.project.services.NeedService;
import com.project.project.model.Need;


@RestController
@RequestMapping("/needs")
@CrossOrigin(origins = "http://localhost:3000")
public class NeedController {
    @Autowired
    NeedService needService;

    @GetMapping
    public List<Need> getAllNeeds() {
        return needService.getAllNeeds();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Need> getNeedById(@PathVariable Long id) {
        Need need = needService.getNeedById(id);
        if (need != null) {
            return ResponseEntity.ok(need);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/byEvent/{event}")
    public List<Need> getNeedByEventId(@PathVariable Long event) {
        return needService.getNeedByEventId(event);
    }

    @PostMapping
    public ResponseEntity<Need> createNeed(@RequestBody Need need) {
        Need createdNeed = needService.createNeed(need);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdNeed);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Need> updateNeed(@PathVariable Long id, @RequestBody Need need) {
        Need updatedNeed = needService.updateNeed(id, need);
        if (updatedNeed != null) {
            return ResponseEntity.ok(updatedNeed);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteNeed(@PathVariable Long id) {
        boolean deleted = needService.deleteNeed(id);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
