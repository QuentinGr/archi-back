package com.project.project.services;

import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import com.project.project.model.Need;
import com.project.project.repository.EventRepository;
import com.project.project.repository.NeedRepository;

@Service
public class NeedService {

    @Autowired
    NeedRepository needRepository;

    @Autowired
    EventRepository eventRepository;

    public List<Need> getAllNeeds() {
        return needRepository.findAll();
    }

    public Need getNeedById(Long id) {
        Optional<Need> optionalNeed = needRepository.findById(id);
        return optionalNeed.orElse(null);
    }

    public List<Need> getNeedByEventId(Long id) {
        return needRepository.getByEventId(id);

    }

    public Need createNeed(Need need) {
        if(need.getEventIdRequest() != null && eventRepository.findById(need.getEventIdRequest()).isPresent()){
            need.setEvent(eventRepository.findById(need.getEventIdRequest()).get());
        }
        return needRepository.save(need);
    }

    public Need updateNeed(Long id, Need updatedNeed) {
        Optional<Need> optionalNeed = needRepository.findById(id);
        if (optionalNeed.isPresent()) {
            Need existingNeed = optionalNeed.get();
            existingNeed.setName(updatedNeed.getName());
            existingNeed.setDescription(updatedNeed.getDescription());
            return needRepository.save(existingNeed);
        } else {
            return null;
        }
    }

    @Transactional
    public boolean deleteNeed(Long id) {
        if (needRepository.existsById(id)) {
            needRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}