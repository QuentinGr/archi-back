package com.project.project.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.project.project.model.Need;


public interface NeedRepository extends JpaRepository<Need, Integer> {

    public List<Need> findAll();

    public Optional<Need> findById(Long id);

    public Boolean existsById(Long id);

    public void deleteById(Long id);

    @Query("select n from Need n where n.event.id = ?1")
    public List<Need> getByEventId(Long id);
}
