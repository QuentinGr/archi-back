package com.project.project.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.project.project.model.Event;


public interface EventRepository extends JpaRepository<Event, Integer> {

    public List<Event> findAll();

    public Optional<Event> findById(Long id);

    public Boolean existsById(Long id);

    public void deleteById(Long id);

    @Query("select e from Event e")
    public List<Event> getEvents();
}
