package com.project.project.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Need {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Event event;

    private String name;
    private String description;
    private Long eventIdRequest;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public Long getEventIdRequest(){
        return this.eventIdRequest;
    }
}
